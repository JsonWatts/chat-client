// Jason Watts Chat Client Code


#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h> 

#define PORT 49153

#define inet_pton
#define strlen

struct sockaddr_in serv; //  main socket variable.
int file_descriptor;
int conn;
char message[100] = ""; //This array will store the messages that are sent by the server.  Read() is not implemented.


int main() {

  file_descriptor = socket(AF_INET, SOCK_STREAM, 0);

  serv.sin_family = AF_INET;

  serv.sin_port = htons(PORT);

  inet_pton(AF_INET, "10.115.20.250", &serv.sin_addr);

  connect(file_descriptor, (struct sockaddr *)&serv, sizeof(serv));

  while(1) {
    printf("Enter a message: ");
    fgets(message, 100, stdin);
    send(file_descriptor, message, strlen(message), 0);
  }

}
